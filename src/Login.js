import { Button } from '@material-ui/core';
import React from 'react';
import './Login.css';
import { auth, provider } from './firebase';

function Login() {
  const singIn = () => {
    auth.signInWithPopup(provider).catch((error) => alert(error.message));
  };

  return (
    <div className="login">
      <div className="login__logo">
        <img src="https://www.shareicon.net/data/2015/08/10/83196_chat_1024x1024.png" alt="login__logo" />
        <h1>iMessage</h1>
      </div>

      <Button onClick={singIn}>Sing In</Button>
    </div>
  );
}

export default Login;
