import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyADnDnGTo916yIde6c9XDkhkd46ouKu7S4',
  authDomain: 'imessage-clone-daee3.firebaseapp.com',
  projectId: 'imessage-clone-daee3',
  storageBucket: 'imessage-clone-daee3.appspot.com',
  messagingSenderId: '131355723942',
  appId: '1:131355723942:web:66fb34f509148502ca4c67',
  measurementId: 'G-RZ4XW398HT',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider };
export default db;
